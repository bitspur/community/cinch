#!/bin/bash

# PostgreSQL credentials and details
PGHOST="localhost"         # Hostname of the PostgreSQL server
PGPORT="5432"         # Port of the PostgreSQL server
PGUSER="postgres"     # Your PostgreSQL username
PGPASSWORD="" # Your PostgreSQL password
DB_TO_RENAME="i_keycloak_accounts" # Database you want to rename
NEW_DB_NAME="i_keycloak_0"  # New name for the database

# Export PostgreSQL credentials
export PGHOST PGPORT PGUSER PGPASSWORD

# Function to terminate connections to a database
terminate_connections() {
    echo "Terminating connections to the database: $1"
    psql -c "SELECT pg_terminate_backend(pg_stat_activity.pid)
             FROM pg_stat_activity
             WHERE pg_stat_activity.datname = '$1'
               AND pid <> pg_backend_pid();"
}

# Function to rename a database
rename_database() {
    echo "Renaming database from $1 to $2"
    psql -c "ALTER DATABASE $1 RENAME TO $2;"
}

# Main script execution
terminate_connections $DB_TO_RENAME
rename_database $DB_TO_RENAME $NEW_DB_NAME

echo "Database renaming completed."
