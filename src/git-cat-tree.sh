#!/bin/sh

exec git cat-file -p $(git cat-file -p $1 | grep -E "^tree" | cut -d ' ' -f 2)
