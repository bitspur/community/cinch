#!/bin/sh

set -e

JSON2YAML=$(which yq 2>&1 >/dev/null && \
    ((yq --version | grep -q "github.com/mikefarah/yq") && echo 'yq eval -P' || echo 'yq -y') || \
    echo 'ruby -ryaml -rjson -e "puts YAML.dump(JSON.parse(STDIN.read))"')

_help() {
    echo "Usage: register-kubeconfig <kubeconfig_file> <cluster_name>"
}

if [ -z "$1" ]; then
    _help
    exit 1
fi
KUBECONFIG_FILE="$1"
if [ -z "$2" ]; then
    _help
    exit 1
fi
CLUSTER_NAME="$2"

if ! command -v jq >/dev/null 2>&1; then
    echo "jq is not installed." >&2
    exit 1
fi
if ! command -v kubectl >/dev/null 2>&1; then
    echo "kubectl is not installed." >&2
    exit 1
fi

export KUBECONFIG="$HOME/.kube/config:$KUBECONFIG_FILE"
mkdir -p "$HOME/.kube"
if [ -f "$HOME/.kube/config" ]; then
    cp "$HOME/.kube/config" "$HOME/.kube/config.bak"
fi
kubectl config view --merge --flatten > "$HOME/.kube/_config"
mv "$HOME/.kube/_config" "$HOME/.kube/config"
NEW_CONTEXT="$(kubectl config get-contexts --kubeconfig="$KUBECONFIG_FILE" -o name | head -n 1)"
NEW_CLUSTER_NAME="$(kubectl config view --kubeconfig="$KUBECONFIG_FILE" -o jsonpath='{.clusters[0].name}')"
NEW_USER_NAME="$(kubectl config view --kubeconfig="$KUBECONFIG_FILE" -o jsonpath='{.users[0].name}')"
kubectl config unset "contexts.$CLUSTER_NAME" >/dev/null
kubectl config unset "clusters.$CLUSTER_NAME" >/dev/null
kubectl config unset "users.$CLUSTER_NAME" >/dev/null
kubectl config rename-context "$NEW_CONTEXT" "$CLUSTER_NAME" >/dev/null
kubectl config view --raw -o json | jq '
  .contexts |= map(select(.name != "'"$CLUSTER_NAME"'")) |
  .clusters |= map(select(.name != "'"$CLUSTER_NAME"'")) |
  .users |= map(select(.name != "'"$CLUSTER_NAME"'")) |
  .contexts[] |= if .name == "'"$NEW_CONTEXT"'" then .name = "'"$CLUSTER_NAME"'" else . end |
  .clusters[] |= if .name == "'"$NEW_CLUSTER_NAME"'" then .name = "'"$CLUSTER_NAME"'" else . end |
  .users[] |= if .name == "'"$NEW_USER_NAME"'" then .name = "'"$CLUSTER_NAME"'" else . end |
  .contexts[] |= if .name == "'"$CLUSTER_NAME"'" then .context.cluster = "'"$CLUSTER_NAME"'" | .context.user = "'"$CLUSTER_NAME"'" else . end
' | $JSON2YAML > "$HOME/.kube/config.tmp"
mv "$HOME/.kube/config.tmp" "$HOME/.kube/config"
kubectl config use-context "$CLUSTER_NAME"
_CONTEXTS=$(kubectl config get-contexts -o name)
for _USER in $(kubectl config view -o jsonpath='{.users[*].name}'); do
    _USER_IN_CONTEXTS=0
    for _CONTEXT in $_CONTEXTS; do
        _CONTEXT_USER="$(kubectl config view -o jsonpath="{.contexts[?(@.name=='$_CONTEXT')].context.user}")"
        if [ "$_CONTEXT_USER" = "$_USER" ]; then
            _USER_IN_CONTEXTS=1
            break
        fi
    done
    if [ "$_USER_IN_CONTEXTS" = "0" ]; then
        kubectl config unset "users.$_USER" >/dev/null
    fi
done
for _CLUSTER in $(kubectl config view -o jsonpath='{.clusters[*].name}'); do
    _CLUSTER_IN_CONTEXTS=0
    for _CONTEXT in $_CONTEXTS; do
        _CONTEXT_CLUSTER="$(kubectl config view -o jsonpath="{.contexts[?(@.name=='$_CONTEXT')].context.cluster}")"
        if [ "$_CONTEXT_CLUSTER" = "$_CLUSTER" ]; then
            _CLUSTER_IN_CONTEXTS=1
            break
        fi
    done
    if [ "$_CLUSTER_IN_CONTEXTS" = "0" ]; then
        kubectl config unset "clusters.$_CLUSTER" >/dev/null
    fi
done
