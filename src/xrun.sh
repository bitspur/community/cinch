#!/bin/sh

unset CLUTTER_BACKEND
export GDK_BACKEND=x11
export QT_QPA_PLATFORM=xcb
export SDL_VIDEODRIVER=x11
export WINIT_UNIX_BACKEND=x11

exec "$@"
