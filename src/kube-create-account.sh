#!/bin/bash

if [ $# -lt 1 ]; then
    echo "Usage: kube-create-account <ServiceAccountName> [Namespace]"
    exit 1
fi

SERVICE_ACCOUNT="$1"
NAMESPACE="$2"
if ! kubectl get serviceaccount "$SERVICE_ACCOUNT" ${NAMESPACE:+--namespace "$NAMESPACE"} >/dev/null 2>&1; then
    echo "Service account $SERVICE_ACCOUNT does not exist in ${NAMESPACE:-default} namespace."
    exit 1
fi
TOKEN="$(kubectl create token "$SERVICE_ACCOUNT" ${NAMESPACE:+--namespace "$NAMESPACE"})"
CLUSTER_NAME="$(kubectl config view --minify -o jsonpath='{.clusters[0].name}')"
APISERVER="$(kubectl config view --minify -o jsonpath='{.clusters[0].cluster.server}')"
CA_CERT="$(kubectl config view --minify --raw -o jsonpath='{.clusters[0].cluster.certificate-authority-data}')"
KUBECONFIG_TEMP=$(mktemp)
cat <<EOF > "$KUBECONFIG_TEMP"
apiVersion: v1
kind: Config
clusters:
- name: $CLUSTER_NAME
  cluster:
    certificate-authority-data: $CA_CERT
    server: $APISERVER
contexts:
- name: $CLUSTER_NAME
  context:
    cluster: $CLUSTER_NAME
    user: $SERVICE_ACCOUNT
current-context: $CLUSTER_NAME
users:
- name: $SERVICE_ACCOUNT
  user:
    token: $TOKEN
EOF
export KUBECONFIG="$KUBECONFIG_TEMP:$HOME/.kube/config"
mkdir -p "$HOME/.kube"
kubectl config view --merge --flatten > "$HOME/.kube/_config"
mv "$HOME/.kube/_config" "$HOME/.kube/config"
rm -f "$KUBECONFIG_TEMP"
