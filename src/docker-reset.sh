#!/bin/sh

docker rm -f $(docker ps -aq)
echo y | docker volume prune -a
echo y | docker network prune
