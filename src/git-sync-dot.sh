#!/bin/sh

DOTDIR="$(dirname "$(pwd)")/.$(basename "$(pwd)")"
CURDIR="$(pwd)"

if [ -d "$CURDIR/.git" ] && [ -d "$DOTDIR/.git" ]; then
    (cd "$DOTDIR" && git pull)
    (cd "$CURDIR" && git pull)
    DOTDIR_REMOTE="$(cd "$DOTDIR" && git remote get-url origin)"
    rm -rf "$DOTDIR/.git"
    rsync -av "$DOTDIR/" "$CURDIR/"
    (cd "$CURDIR" && git add . && git commit)
    cp -r "$CURDIR/.git" "$DOTDIR/"
    (cd "$DOTDIR" && git remote set-url origin "$DOTDIR_REMOTE")
fi
