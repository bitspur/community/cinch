#!/bin/sh

DRY_RUN="${DRY_RUN:-0}"
GITLAB_TOKEN="$(gitlab-token)"
GITLAB_URL="${GITLAB_URL:-https://gitlab.com}"
GROUP_NAME="${GROUP_NAME:-$1}"
if [ -z "$GROUP_NAME" ]; then
    echo "group name is required" >&2
    exit 1
fi
GROUP_ID=$(curl -s --header "Authorization: Bearer $GITLAB_TOKEN" "$GITLAB_URL/api/v4/groups/$GROUP_NAME" | jq -r '.id')
DESTINATION_PATH="${DESTINATION_PATH:-$2}"
if [ -z "$DESTINATION_PATH" ]; then
    DESTINATION_PATH="$HOME/Projectz/$GROUP_NAME"
fi
mkdir -p "$DESTINATION_PATH"
DESTINATION_PATH="$(realpath "$DESTINATION_PATH")"

echo "GITLAB_URL: $GITLAB_URL"
echo "GROUP_NAME: $GROUP_NAME"
echo "GROUP_ID: $GROUP_ID"
echo "DESTINATION_PATH: $DESTINATION_PATH"

_exec() {
    echo "\$ $@"
    if [ "$DRY_RUN" != "1" ]; then
        "$@"
    fi
}

_list_and_clone_projects() {
    group_id=$1
    parent_path=$2
    project_page="1"
    group_name=$(curl -s --header "Authorization: Bearer $GITLAB_TOKEN" "$GITLAB_URL/api/v4/groups/$group_id" | jq -r '.name')
    echo "========== $group_name =========="
    while response="$(curl -s --header "Authorization: Bearer $GITLAB_TOKEN" "$GITLAB_URL/api/v4/groups/$group_id/projects?per_page=100&page=$project_page")"; \
            [ "$response" != "[]" ]; do
        project_page="$((project_page+1))"
        if [ -z "$response" ]; then
            echo "$response" >&2
            return
        fi
        echo "$response" | jq -r '.[] | .path_with_namespace' | while read project_path; do
            echo "--- $project_path ---"
            dir="$parent_path/$(echo $project_path | sed "s|^$GROUP_NAME/||")"
            git_url=$(echo "$response" | jq -r '.[] | select(.path_with_namespace=="'$project_path'") | .ssh_url_to_repo')
            if [ -d "$dir" ]; then
                _exec git -C "$dir" pull
            else
                _exec mkdir -p "$(dirname "$dir")"
                _exec git clone "$git_url" "$dir"
            fi
        done
    done
    group_page="1"
    while response=$(curl -s --header "Authorization: Bearer $GITLAB_TOKEN" "$GITLAB_URL/api/v4/groups/$group_id/subgroups?per_page=100&page=$group_page"); [ "$response" != "[]" ]; do
        group_page="$((group_page+1))"
        echo "$response" | jq -r '.[] | .id' | while read subgroup_id; do
            _list_and_clone_projects "$subgroup_id" "$parent_path"
        done
    done
}

_exec git lfs install
_list_and_clone_projects "$GROUP_ID" "$DESTINATION_PATH"
