#!/bin/sh

BASE64_NOWRAP="openssl base64 -A"
AUTH=$(cat ~/.docker/config.json | jq -r '.auths["registry.gitlab.com"].auth')

TOKEN=""
if [ "$AUTH" != "" ] && [ "$AUTH" != "null" ]; then
    TOKEN=$("$AUTH" | $BASE64_NOWRAP -d | cut -d':' -f2)
else
    DOCKER_CREDENTIAL=$(echo docker-credential-$(cat ~/.docker/config.json | jq -r '.credsStore'))
    if which $DOCKER_CREDENTIAL 2>/dev/null >/dev/null; then
        TOKEN=$(echo registry.gitlab.com | $DOCKER_CREDENTIAL get | jq -r '.Secret')
    fi
fi
echo $TOKEN
