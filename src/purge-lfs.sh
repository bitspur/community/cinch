#!/bin/sh

if [ ! -d .git ]; then
    echo "must be in the root of a git project" >&2
    exit 1
fi
if ! which git 2>&1 >/dev/null; then
    echo "git not installed" >&2
    exit 1
fi
if ! which git-lfs 2>&1 >/dev/null; then
    echo "git-lfs not installed" >&2
    exit 1
fi
if ! which git-filter-repo 2>&1 >/dev/null; then
    echo "git-filter-repo not installed" >&2
    exit 1
fi

lfs_ls_all_files() {
    for c in $(git log --all --pretty=format:'%H'); do
        git lfs ls-files "$c"
    done
}

ORIGIN=$(git remote get-url origin)
git add -A
git reset --hard
git clean -fxd
eval $(echo git filter-repo $(lfs_ls_all_files | cut -d' ' -f3 | sed "s|^|--path '|g" | sed "s|$|'|g") --invert-paths --force)
if [ -f .gitattributes ]; then
    git filter-repo --path .gitattributes --invert-paths --force
fi
git lfs prune --force
git add -A
if ! git remote get-url origin >/dev/null 2>/dev/null; then
    git remote add origin $ORIGIN
fi
if [ "$(find .git/lfs/objects -type f)" != "" ]; then
    echo "failed to purge lfs" >&2
    exit 1
fi
