#!/bin/sh

YAML2JSON=$(which yq 2>&1 >/dev/null && \
    ((yq --version | grep -q "github.com/mikefarah/yq") && echo 'yq -o json' || echo yq) || \
    echo 'ruby -ryaml -rjson -e "puts JSON.pretty_generate(YAML.load(ARGF))"')
JSON2YAML=$(which yq 2>&1 >/dev/null && \
    ((yq --version | grep -q "github.com/mikefarah/yq") && echo 'yq eval -P' || echo 'yq -y') || \
    echo 'ruby -ryaml -rjson -e "puts YAML.dump(JSON.parse(STDIN.read))"')

_help() {
    echo "Usage: k8s-login <proxmox_host> <cluster_host>"
}

cleanup() {
    rm -f "$TEMP_CONFIG_FILE" "$TEMP_CONFIG_FILE.tmp" >/dev/null 2>&1
}

if [ "$1" = "" ] || [ "$2" = "" ]; then
    _help
    exit 1
fi

PROXMOX_HOST="$1"
CLUSTER_HOST="$2"
TEMP_CONFIG_FILE=$(mktemp)
trap cleanup EXIT INT TERM HUP QUIT
if ! scp "admin@$PROXMOX_HOST:/home/admin/.kube/config" "$TEMP_CONFIG_FILE"; then
    echo "failed to copy kube config from $PROXMOX_HOST"
    exit 1
fi
if echo "$2" | grep -E '^([0-9]{1,3}\.){3}[0-9]{1,3}$|^([0-9a-fA-F]{0,4}:){1,7}[0-9a-fA-F]{0,4}$' >/dev/null; then
    CLUSTER_IP="$2"
else
    CLUSTER_IP=$(nslookup "$2" | awk '/^Address: / { print $2 }')
    if [ -z "$CLUSTER_IP" ]; then
        echo "failed to resolve cluster host: $2"
        exit 1
    fi
fi
cat "$TEMP_CONFIG_FILE" | $YAML2JSON | \
    jq '(.clusters[] | select(.cluster.server) | .cluster.server) |= "https://'"$CLUSTER_IP"':6443"' | \
    $JSON2YAML > "$TEMP_CONFIG_FILE.tmp" && mv "$TEMP_CONFIG_FILE.tmp" "$TEMP_CONFIG_FILE"
register-kubeconfig "$TEMP_CONFIG_FILE" "$CLUSTER_HOST"
