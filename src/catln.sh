#!/bin/sh

exec cat $1 | nl -ba | grep --color=always -C${3:-10} -E "^\s*${2:-1}\s"
