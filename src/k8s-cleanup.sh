#!/bin/sh

kubectl get pods --all-namespaces -o json | jq -r '
    .items[] |
    select(.status.phase == "Failed" or .metadata.deletionTimestamp != null) |
    "\(.metadata.namespace) \(.metadata.name) \(.status.phase)"
' | while read _NAMESPACE _POD_NAME phase; do
    if [ "$phase" = "Failed" ]; then
        kubectl delete pod $_POD_NAME -n $_NAMESPACE
    else
        kubectl delete pod $_POD_NAME -n $_NAMESPACE --force --grace-period=0
    fi
done
